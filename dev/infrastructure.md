---
title: Infrastructure
description: 
published: true
date: 2020-10-13T16:55:42.184Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:40.345Z
---

OrangeFox Recovery's infrastructure list:

- wikijs: OrangeFox Wiki, serving [wiki.orangefox.tech](https://wiki.orangefox.tech), [open-sourced](https://wiki.js.org/)
- posteio: Mail server, serving [mail.orangefox.tech](mail.orangefox.tech), [open-sourced](https://poste.io)
- fox-h5ai: File lister, serving [files.orangefox.tech](https://files.orangefox.tech), [open-sourced](https://gitlab.com/OrangeFox/site/h5ai-dockered)
- mPanel: Panel for maintainers for add/edit devices and releases, serving [mpanel.orangefox.download](https://mpanel.orangefox.download), closed-sourced
- oAPI1: API server, serving [api.orangefox.download/v1](api.orangefox.download/v1), [API documentation](/dev/api), closed-sourced
- oAPI2: API server, serving [api.orangefox.download/v2](api.orangefox.download/v2), [API documentation](/dev/api), closed-sourced
- oBot: Telegram chat-bot ([@ofoxr_bot](https://t.me/ofoxr_bot)), [open-sourced](https://gitlab.com/OrangeFox/site/obot)

{.grid-list}