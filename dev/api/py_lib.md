---
title: Official Python asynchronous library
description: 
published: true
date: 2020-08-02T14:17:56.064Z
tags: 
editor: markdown
dateCreated: 2020-08-02T14:17:51.847Z
---

OrangeFox is having official python library.
Sources - [here](https://gitlab.com/OrangeFox/infrastructure/python-api-lib)

## Installing
``` bash
	pip install orangefoxapi --index-url https://gitlab.com/api/v4/projects/20272543/packages/pypi/simple -U
```

## Including in your requirements.txt
```
	--index-url https://gitlab.com/api/v4/projects/20272543/packages/pypi/simple
```

## Avaible methods under OrangeFoxAPI class
- async def ping() -> bool
- async def list_oems() -> list
- async def get_oem(oem: str, only_codenames=False) -> list
- async def list_devices(only_codenames=False) -> list
- async def get_device(codename: str) -> dict
- async def get_devices_with_releases(release_type=None) -> dict
- async def get_oem_devices_with_releases(oem_name, release_type=None) -> dict
- async def get_release(release_id: str) -> dict
- async def get_last_release(release_type=None) -> dict
- async def get_device_release(codename: str, release_type=None, version=None) -> dict
- async def get_updates(var: Union[str, int]) -> list
- async def get_device_updates(codename: str, var: Union[str, int], build_type=None) -> list


## Examples
``` python
	from orangefoxapi import OrangeFoxAPI
  
	api = OrangeFoxAPI()
  
  print(await api.ping())
```