---
title: Encryption
description: 
published: true
date: 2024-02-10T16:36:11.794Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:00:09.636Z
---

## Will OrangeFox encrypt my phone?
+ No. OrangeFox (or any other recovery) **cannot** encrypt anything. Encryption is done by your **ROM**.
+ So, if you ever see anybody claiming that OrangeFox encrypted their phone, please tell them that this is <u>absolutely impossible</u> - because only a ROM can encrypt a phone.

## Every time I flash a ROM, my phone gets encrypted
+ Good! And so it should. This is a `very good` security measure, and you `should` keep your phone encrypted.
+ If you do anything to make your device remain unencrypted (eg, by flashing a "DFE" zip), then know that `you are on your own`. The whole purpose of a DFE zip is to deliberately break the encryption protocols on your device, so that they do not work as designed. It may all `seem` to be working okay. But trouble is often not far away. Do `not` come and ask any questions about whatever you experience. 

## I am using OrangeFox, and I am being asked for a password (or PIN). But I have not set a password (or PIN)
+ If your device has FBE encryption and you have set up a password/PIN in the ROM (or if it has FDE encryption, and you enabled Secure Startup in the ROM) then you will be asked for a password (or PIN, or whatever) - just supply the ROM's lockscreen password/PIN.
+ If you did none of the above, then this is usually a sign of broken encryption (you may have flashed a ROM which has different encryption protocols from the ROM that you had before, or you may have flashed a firmware zip which uses an incompatible keymaster module - or whatever other reason).
+ If this is the case, then you will need to <u>format your data partition</u> (which will also wipe all the contents of your internal storage). If you have no backups, then that would be most unfortunate.

## I have set a pattern/PIN but OrangeFox is asking for a password
+ If you experience a situation where you have set a pattern/PIN, but the recovery asks for a password (or vice versa, etc), then you must ask your ROM devs to fix their ROM by applying [this patch](https://android.googlesource.com/platform/system/vold/+/ca08c0a724856e9c95a679991167afd297eda915%5E%21/). 
+ This is not a recovery issue, so please do not ask us to "fix" it. 
+ Alternatively, you can always open a terminal in the recovery (or via `adb shell`) and run this command - *twrp decrypt password*.

## My file names are all gibberish
+ That is a sign of broken encryption. This can be caused by any number of things. 
+ The real solution is to get the correct version of OrangeFox for your ROM. 
+ You might also have to format your data partition in order to try and correct anything that is broken in your device's encrpytion.

## I am trying to format my data partition, but I am getting errors
+ This sometimes happens. If it does happen, then wipe the data partition, then reboot OrangeFox immediately, and then try formatting data again. 
+ If this doesn't solve the problem, then try the following steps:
1. Connect your device to your PC via usb cable;
2. Reboot to bootloader;
3. Run the command: `fastboot -w`
4. Reboot to OrangeFox Recovery immediately;
5. Format data again.

## I have other encryption issues
Encryption issues may be caused by several factors. Many encryption problems can only be solved by <u>formatting</u> (not just wiping) the data partition.

Here is an inexhaustive list of possible causes;
+ You replaced a ROM with a different ROM (or a different version of the same ROM) that uses a different encryption protocol (<b>solution</b> = format your data partition)
+ You flashed a firmware zip, either directly, or via flashing a MIUI ROM, and the firmware version is incompatible with your device's current encryption (<b>solution</b> = format your data partition and/or flash a firmware zip that is compatible with your encryption)
+ Your ROM's encryption protocol is unsupported (<b>solution</b> = format your data partition, and keep your device unencrypted) 
+ You flashed or booted an Android 12+ recovery release on an Android version that is lower than Android 12 (<b>solution</b> = format your data partition; sometimes you might also need to flash your ROM again)
+ You restored backed up data, and something went wrong with the restoration (<b>solution</b> = format your data partition; sometimes you might also need to flash your ROM again)
+ Any number of other possible causes (<b>solution</b> = format your data partition; sometimes you might also need to flash your ROM again)

## I have an OrangeFox "A12" release that is supposed to support Android 12/13/14, but decryption fails
+ First of all, make sure that you actually have the correct OrangeFox release for your ROM (eg, if you have flashed a ROM with FBE v2 encryption, and you are using an OrangeFox release for FBE v1 (or vice versa)), then you need to change to the correct version.
+ If you are sure that you have the correct OrangeFox release for your current ROM, and you formatted the data partition after flashing the ROM, then try the following steps:
1. Reboot to Android;
2. Remove your lockscreen PIN/password protection;
3. Reboot Android;
4. Create the lockscreen protection again;
5. Reboot to OrangeFox.
+ If none of these solves the problem, then you need to report in the relevant OrangeFox Telegram group, and make sure that you post both the recovery log and the logcat log.
