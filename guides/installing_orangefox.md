---
title: Installing OrangeFox Recovery
description: 
published: true
date: 2024-01-15T22:39:33.152Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:00:18.884Z
---

## How do I install OrangeFox?
## 0. Firstly, you <u>MUST</u> unlock your bootloader. Then you can proceed in one of the ways described below.

## 1. Via a recovery
+ Unlock your bootloader (if not already unlocked)
+ To install an OrangeFox `zip`, simply flash the OrangeFox zip with OrangeFox itself, or with any TWRP-compatible custom recovery, without any wipes - exactly the same way that you would flash any other zip.
+ Do the same thing if you want to upgrade (or downgrade) OrangeFox (<u>see further details about upgrading OrangeFox below</u>).
+ When you flash an OrangeFox zip, there is no need to flash any DFE or any other such thing.
+ Always ensure that you are flashing the correct OrangeFox zip for the ROM that is running on your device.
+ See below for detailed instructions on how to install OrangeFox via fastboot.
+ Do `NOT ever` try to flash the OrangeFox zip via fastboot. The zip must be flashed with a recovery.

## Detailed instructions on how flash OrangeFox Recovery
## 2. Via fastboot, and then recovery

You `need a PC for this`. If you do not have a PC, or access to a PC, then you are stuck!
*Note - these instructions are mainly for A-only devices*:
*Note 2 - do NOT `EVER` use fastboot to flash a recovery zip file*:

+ Unlock your bootloader (if not already unlocked)
+ Install adb, fastboot [from here](https://developer.android.com/studio/releases/platform-tools#downloads), and the relevant USB drivers onto your PC
+ Download the correct OrangeFox zip file to your phone, and to your PC
+ Extract `recovery.img` from the OrangeFox zip file, and copy `recovery.img` to your PC’s adb directory
+ Reboot your phone into bootloader mode
+ Open up a command line window / terminal emulator on your PC
+ Change to the adb directory on your PC 
+ If you have an `A-only` device, or `an A/B device that has a dedicated recovery partition`, flash OrangeFox Recovery by running: `fastboot flash recovery recovery.img` (or whatever command your device's maintainer has recommended, if that is different). Then Reboot into OrangeFox by pressing the `power + volume up` keys, until you see the OrangeFox splash screen (the keys may be may vary on your device)
+ If you have an `A/B` or `Virtual A/B` device (eg, Poco F3 or Poco F4), boot OrangeFox Recovery by running: `fastboot boot recovery.img`
+ If your ROM uses `vendor_boot-as-recovery`, then <u>none</u> of these will work; you will require special assistance and instructions from your ROM's maintainer
+ After OrangeFox has booted up, check that everything is working – eg, that it has mounted the data partition successfully, and that the touchscreen works.
+ Find and `select the OrangeFox zip` on your phone, tap on it, and `swipe to install it (because OrangeFox Recovery needs some files from the zip)`.
+ After installation, the phone will automatically reboot into OrangeFox
+ Enjoy!

## Should I backup my device before flashing/upgrading/changing my recovery?
+ Yes. Yes. YES. YES. 
+ Be sure to make `full backups of your data partition and internal storage` before flashing or upgrading a recovery. 
+ Do this `every single time` you want to flash or upgrade a recovery.
+ By doing this, you can secure yourself against potential problems. 
+ If you do not make these backups, and something goes wroing with the installation, `you will be on your own`.

## How can I find out whether my phone is an A/B device?
+ Connect to your phone with a usb cable, open a command prompt, and run the command: 
`adb shell getprop ro.boot.slot_suffix`
+ If your phone is an A/B device, you will be shown the active slot (ie, either "_a", or "_b"). If the command shows no output, then the phone is NOT an A/B device.

## How do I upgrade OrangeFox?
+ If you want to change your OrangeFox version, first, you should remove any OrangeFox password/PIN protection that you have set in OrangeFox itself. <u>Do NOT skip this step</u>;
+ Then flash the new/replacement OrangeFox zip with OrangeFox, in the same way that you would flash any zip

## Can I run "fastboot boot ..." to boot an OrangeFox .img file?
+ If you have an `A/B` or `Virtual A/B` device, then you will be able to do this. But if your ROM uses `vendor_boot-as-recovery`, you will not be able to do this.
+ For `A-only devices`, see below:
+ Some devices allow you to "hotboot" a recovery image, without actually flashing it to the device. This is done via "`fastboot boot <recoveryimage.img>`". 
+ Old Xiaomi devices, running old MIUI firmwares (ie, based on Android 6.x, 7.x or 8.x) are typical devices that allow this. 
+ Some new A-only Xiaomi devices also allow "hotbooting" a recovery image (eg, miatoll, vayu, etc).
+ For some older A-only Xiaomi devices (eg, violet, lavender), if your MIUI firmware is based on Android 9 or later, then it might not be possible. Why? Because newer Xiaomi firmware updates in newer MIUI ROMs (based on Pie or later) <u>will replace your bootloader</u> with one that cannot "hotboot" a recovery image in this way.
+ Your only option in such a case is to <u>flash</u> the image, with "fastboot flash ..."
+ This is not an OrangeFox issue, so please do <u>not</u> come to the OrangeFox groups to ask about it.

## Can I use OrangeFox's own password feature?
+ Of course. Just be sure to <u>NEVER, EVER</u> forget your password/PIN. If you do, `you will be on your own`.

## Do I really need to flash the OrangeFox zip?
+ Yes. Yes. YES. YES. You really `MUST` flash the OrangeFox zip.
+ Do you really think that we release OrangeFox in zip format, just for the sake of it, or just because we do not know what we are doing?
+ If you do not flash the zip, and you get problems, `you will be on your own`.
