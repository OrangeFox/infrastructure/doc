---
title: OrangeFox Recovery API
description: 
published: true
date: 2024-04-20T18:57:05.843Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:31.530Z
---

OrangeFox Recovery is having its own API to get supported devices/releases/updates and other info. You can build your sites/apps using it.

> On 15th Apr 2024, the OrangeFox team introduced the new OrangeFox API v4.
> In difference to previous one, this won't have a version prefix, but instead will be developed continuously to ensure the newest changes.
{.is-info}

> **New API schedule**
> 15th Apr 2024 - new API at api.orangefox.download/ created
> 15th Apr 2024 - old API at api.orangefox.download/old/ created
> 20th Apr 2024 - new API at api.orangefox.download/v3/ propagated
> 11th May 2024 - new API at api.orangefox.download/old/ propagated
{.is-warning}


### New OrangeFox API changes
Please check out its swagger for notes and documentation - https://api.orangefox.download/redoc
The new OrangeFox is almost completely backwards compatible, except these:

#### Removed
* Users/maintainers endpoints removed
* Releases sorting modes removed

#### Added
* `archived` releases support
* `build_id` releases support

#### Endpoints deprecations
* ~~* `/devices/get` and `/releases/get` are depreacted *~~
* `/updates/` is deprecated, please use `/releases/?after_release_id=` as a drop-in replacement instead.

#### Parameters deprecations
* `_id` parameters was changed to `id` eveywhere to ensure consistency, the old ones wasn't removed thought for compatiblity reasons and considered deprecated/
* `/releases/?codename=` is depreacted, please use device ID instead
* `/devices/?model_name=` is depreacted, please use device ID or codename instead

#### JSON Body response model deprecations
* `_id` parameters was changed to `id` eveywhere to ensure consistency, the old ones wasn't removed thought for compatiblity reasons and considered deprecated.
* `model_name` and `codename` for both `/devices` and `/devices/<id>` endpoints are depreacted. We are introduced `model_names` and `codenames` which would contain multiple items in future.
* `size` in `/release/<id>` is deprecated

--- 

- [v3 - API docs](https://api.orangefox.download/v3/docs)
- [Current API docs (Beta)](https://api.orangefox.download/redoc)
{.links-list}

## API Libraries
- [Official Python lib](https://gitlab.com/OrangeFox/infrastructure/python-api-lib)
{.links-list}

## Example of usage OrangeFox API in projects
name | language
- [OrangeFox Telegram bot (oBot) Python](https://gitlab.com/OrangeFox/site/obot)
- [OrangeFox downloads website (dSite) | NodeJS / React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### Third-party
- [Friendly telegram userbot module (FTG) | v2 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}

## Notes
### Special mirror names and its definitions
By using `/release/{id}` method you will see a mirrors field, it contains download mirrors, usually they are named in the [Alpha 2 (ISO 3166) way](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) (for example `US` and `NL`), but there is some reserved named with other definition:
- DL: Global mirror 🌎
- AR: Archieve 🗃
