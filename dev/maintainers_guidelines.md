---
title: Gudelines for official maintainers
description: 
published: true
date: 2024-01-15T18:34:37.063Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:44.852Z
---

+ Being an official OrangeFox maintainer is a <u>very serious responsibility</u>. 
+ It is <u>much more</u> than just building and releasing, and then disappearing for long periods. 
+ It requires very regular and very active maintenance, including dealing with any reports/issues about your device, in the OrangeFox telegram groups/chats and on XDA.
+ Here you can find some simple rules which you should follow.

> :warning: These rules apply to OrangeFox's official maintainers. If you want to apply to become an official maintainer, [follow this guide](/dev/maintainerships)
{.is-warning}

> These rules relate to OrangeFox Recovery R11.1. For futures versions, the rules may be changed.
{.is-info}


### Official builds
#### Build type
All official builds should have a build type field, it should be `Stable`, `Beta` or `Untested`. These are case-sensitive. All other builds will be considered as unofficial.

#### Version
You are not allowed to change the OrangeFox Recovery version number. But you can set maintainer's release. For example:
`export FOX_VERSION=R11.1_1` where 'R11.1' is the OrangeFox version, '_1' is a maintainer's release. For every new OrangeFox build with same version you should increase maintainer's release by 1.

The current OrangeFox version is R11.1

> :warning: You cannot release more than one build with the same FOX_VERSION!
{.is-warning}

### No modifications
+ Maintainers must `not` modify `anything` in the OrangeFox source code or vendor tree. Every release must be based on the pristine sources only.

### Proper testing
This is the most important point. Every build must be properly tested before release. It is not sufficient to have tested a previous build. The exact build that you wish to release must first be tested.
* Every release build must be tested with [this](/dev/maintainerships) test suite. 
* Beta builds do not have to pass the entire test suite - but they must boot correctly, basic features must work, and they must support flashing other OrangeFox Installation zips and imgs (for rolling back to a stable version if something is broken in the beta).
+ Proper testing necessarily involves flashing and testing both stock ROMs and custom ROMs. 
+ If there are actively maintained official ROMs (both stock and custom) based on different Android versions (eg, Android 11, Android 12.x, Android 13, etc), then a selection of all those ROMs must be flashed and tested, so that any general issues relating to specific Android versions can be identified in advance, and solutions or work-arounds developed - before a final release build is made (or at the very least so that the issues can be properly documented in the release notes of known issues/bugs).

### Making releases
+ Every official OrangeFox release <u>must be done via the OrangeFox mpanel</u>. It is not permitted to make an official release in any other manner.
+ All official release zips must be signed during the build process. It is not permitted to make a release of an unsgined zip.
+ The <u>full URL</u> for the official OrangeFox download site must always be provided in every place where an official release is being promoted or referred to. Links must <u>not</u> be shortened with bit.ly or other similar services.
+ Maintainers must not make, promote, or support any unofficial release for a device that they are maintaining, or for any device for which there is an official maintainer.
+ Every release must be accompanied by pushing an update of the device tree to the OrangeFox gitlab repo, so that the device tree on the OrangeFox gitlab repo is the <u>exact</u> device tree that was used in compiling the release build.
+ Please keep a copy of `every` zip file that you have released, and please be ready to provide this in case we need a backup copy.


### Doing the work of a maintainer
+ Maintainers should always strive to do their work in a responsible and professional manner, and must show high levels of dedication, faithfulness, and commitment, to the role, and to the OrangeFox project.
+ Maintainers must <u>actively</u> support their releases in the OrangeFox telegram chats.
+ Maintainers must absolutely maintain the confidentiality of all team discussions. There are no exceptions to this rule. 
+ Maintainers must not discuss anything or raise any questions that relate to the role of maintainer in any public chat or channel. Any such discussion must be conducted in private team chats.
+ Maintainers must not run public parallel/alternative telegram groups/chats that discuss OrangeFox issues. All public discussions relating to OrangeFox must be done in the official OrangeFox groups/chats.
+ Maintainers must create a thread on XDA for every device that they are maintainer for.
+ Maintainers must <u>actively</u> support the XDA threads for all their devices (ie, not just creating a thread and then disappearing).
+ As a maintainer, you cannot expect others to answer questions about devices that you are maintaining. It is your responsibility to do that.
+ This means that you must be very visible in the official OrangeFox telegram main support group (for Stable releases) and in the official OrangeFox Beta testing group (for Beta releases).
+ Maintainers must read carefully, and become familiar with, `all the contents` of the OrangeFox wiki. This will enable you to provide accurate advice and to avoid mistakes.
+ If a maintainer feels that he/she can no longer do the role in the expected manner and to the expected level of professionalism, or to demonstrate the required levels of loyalty and faithfulness to the OrangeFox project, he/she should formally resign from the position by contacting MrYacha.
+ Any maintainer who does not fulfil properly the role of a maintainer, as has been described here, is liable to be removed from that position after one warning. The warning may be issued personally to the individual maintainer, or be posted generally for the attention of all maintainers.

### Core ground rules
Every OrangeFox release must look and behave the same way, no matter the device. Therefore all maintainers should follow these ground rules:

#### Fstab
A. The MicroSD (if the device supports it) must be mounted in fstab as "/sdcard1"
+ Example: 
/sdcard1 vfat /dev/block/mmcblk1p1 /dev/block/mmcblk1 flags=fsflags=utf8;display="MicroSD";storage;wipeingui;removable

B. The USB OTG must be mounted in fstab as "/usb_otg", and a display flag of "USB-Storage"
+ Example:
/usb_otg vfat /dev/block/sdg1 /dev/block/sdg flags=fsflags=utf8;display="USB-Storage";storage;wipeingui;removable

C. The fstab must provide for backup of internal storage (mounted as "/storage")
+ Example:
/storage ext4 /data/media/0 flags=display="Internal Storage";usermrf;backup=1;fsflags="bind";removable

D. The system_image and vendor_image partitions (on devices without dynamic partitions only) should have the "backup=1" flag in fstab
+ Examples:
/system_image emmc /dev/block/bootdevice/by-name/system flags=backup=1;flashimg=1
/vendor_image emmc /dev/block/bootdevice/by-name/vendor flags=backup=1;flashimg=1

E. On devices with dynamic partitions, the fstab must NOT provide any interface for backing up, restoring, or flashing to, individual dynamic partitions. Only the Super partition must be available for backup/restore (and this is already done automatically, so it does not need to be done in the fstab).


#### OrangeFox variables
**The variables list is [here](https://gitlab.com/OrangeFox/vendor/recovery/-/blob/master/orangefox_build_vars.txt)**
+ If your device has a non-standard resolution (i.e., it is not 1920x1080) you should export `OF_SCREEN_H`
+ If your device has round corners, cutouts, or notches, you should export the correct vars from the list. Setting offset to drop notch space is not allowed!

### Sources
+ As already indicated above, the OrangeFox sources that are compiled for each release must be the pristine sources on the OrangeFox gitlab repo.
+ You must `not` change the OrangeFox sources for any release without prior authorisation from the developers.
+ It is highly unlikely that the source code would require changes just for a specific device to work properly. So it would be most unlikely that source code changes would be accepted just to satisfy one maintainer preferences, or just for one device.

### Trees maintaining
You should maintain a device/kernel tree on [our GitLab page](https://gitlab.com/OrangeFox).

#### Branches naming
The device/kernel repository should have default a fox_(build-manifest version) branch - for example, fox_12.1. It should have been synced with most up-to-date release source, and, if you are working on a new release you should push it in master.

### Keep updated
Your device should receive the latest OrangeFox source code updates and important commits. Also, you should pick the latest device tree fixes/improvements from TWRP or TWRP's forks projects device trees.

### Support
You should provide your build's users with support on [our Telegram chat](https://t.me/OrangeFoxChat), and on XDA.

### Testing upcoming releases
Testing upcoming releases from our closed repository are highly recommended. In order to be able to access the closed repository, you should have enabled 2FA on your GitLab profile.

> :warning: Please do not lose your GitLab's 2FA keys. Making backups of 2FA keys is always good idea.
{.is-warning}


## Maintaining a new device
+ First you should follow all the other rules on this page. Otherwise, you will not be allowed to maintain another device, as you cannot yet handle and support the current one(s) properly.