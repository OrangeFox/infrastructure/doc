---
title: Инфраструктура
description: 
published: true
date: 2020-10-13T16:57:08.658Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:09:08.768Z
---

Список объектов инфраструктуры OrangeFox Recovery:

- wikijs: OrangeFox Wiki, хостится на [wiki.orangefox.tech](https://wiki.orangefox.tech), [open-sourced](https://wiki.js.org/)
- posteio: почтовый сервер, хостится на [mail.orangefox.tech](mail.orangefox.tech), [open-sourced](https://poste.io)
- fox-h5ai: Хранилище файлов, хостится на [files.orangefox.tech](https://files.orangefox.tech), [open-sourced](https://gitlab.com/OrangeFox/site/h5ai-dockered)
- oFilesServer: сервер статических файлов, предоставляющий файлы на [files.orangefox.tech](https://files.orangefox.tech), closed-sourced
- mPanel: Панель для сопровождающих для добавления/редактирования устройств и релизов, хостится на [mpanel.orangefox.tech](https://mpanel.orangefox.tech), closed-sourced
- oAPI: сервер API, хостится на [api.orangefox.tech](https://api.orangefox.tech), [API документация](/dev/api), closed-sourced
- oBot: Telegram чат-бот ([@ofoxr_bot](https://t.me/ofoxr_bot)), [open-sourced](https://gitlab.com/OrangeFox/site/obot)
- oSite: Сайт OrangeFox Recovery хостится на [orangefox.tech](https://orangefox.tech), closed-sourced
{.grid-list}