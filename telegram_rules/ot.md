---
title: Offtopic chats rules
description: 
published: true
date: 2021-01-28T16:08:28.239Z
tags: 
editor: markdown
dateCreated: 2021-01-28T16:07:29.086Z
---

> Since the most of general rules can't be applied on offtopic chats we have our own ones that will cover the most defenseless points.
{.is-info}


## Offtopic is a offtopic
Please be aware of asking on-topic questions, you can get unwanted/not correct answers. We will never guarantee any accuracy of anything in OT chats.


## Offtopic is not the same as flooding
[Flooding]() is completely restricted in OT chats. If you [can't handly myself](https://psychology.wikia.org/wiki/Flooding) then you won't be able to speak in OT chats.


## Be tolerant with all participates of the chat.
Insults will directly issue a warning from our side and you can be miffed.


> There are no rules that can cover any cases of users' behavior admins take the right to make their own decisions. Regarding of [general rules](/telegram_rules), admin's decisions are not debatable.
{.is-warning}
