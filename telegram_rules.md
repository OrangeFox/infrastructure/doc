---
title: Rules for OrangeFox telegram and discord groups
description: 
published: true
date: 2024-01-30T16:58:40.408Z
tags: 
editor: markdown
dateCreated: 2020-08-27T12:39:42.744Z
---

# OrangeFox forum rules
These are the OrangeFox forum rules. Read them very carefully. Adhere to `the letter and the spirit` of the rules.

> -These rules will be applied strictly. Please take them very seriously.
> -Be sure to familiarise yourself with all the rules before posting anything.
> -Breaking a rule will get you a warning. Trying "cleverly" to circumvent the rules will also get you a warning.
> -Breaking some rules may attract an automatic ban, potentially from all our groups.
> -If you receive 3 warnings, this will result automatically in a ban.
> -Ignorance of the rules will not be accepted as an excuse.
{.is-warning}


## 1. Do not ever ask about ETAs (Estimated Time of Arrival)
It is rude to ask any of the following questions, whether directly, or indirectly:
- whether (or when) something will be released
- whether (or when) a particular device (or feature) will be updated
- whether (or when) a particular device (or feature) will be supported
- whether "somebody" (or anybody) is working on a build for a particular device
- whether there is any support for a particular device
- whether there is support, or source code, or development, or a particular Android version
- why a particular device, or feature, or Android version is not supported
- whether (or when) there will be an update to match any other recovery (or any of its versions) or any Android version
- or anything else along these lines

It is forbidden to suggest/ask/beg/say, directly, or indirectly, that:
- I need OrangeFox for a particular device
- I hope that OrangeFox will be built for a particular device
- Please make OrangeFox for a particular device
- Please support this or that Android version
- Please update OrangeFox for a particular device (or for a particular Android version)
- Please add this or that feature to OrangeFox
- or anything else along these lines

So: 
- If you want to know whether a specific device is currently supported, go to the OrangeFox download site - https://orangefox.download and search for the device there
- If the device is not in the download site, then it is not currently supported. Do <u>NOT</u> ask any question about it
- If you do ask any question about it (or otherwise refer to it) you will have infringed <u>two</u> rules, namely, Rule #1 (ie, this rule), and Rule #3 (off-topic); this may attract <u>two warnings</u> and/or an immediate ban
- Do NOT come to the groups to try and use any surreptitious means (directly or indirectly) to ask about a device. If you do, you will get a warning
- Be sure to <u>read all the pinned messages</u> in any group before you post anything about any device. If you fail to do this, and there is a relevant pinned message, you <u>will</u> get a warning.

## 2. Applicable to all OrangeFox groups, unless otherwise stated
- These rules apply to all OrangeFox groups, chats and channels, whether on Telegram, or Discord, or any other platform
- Some of the rules do not apply to the Off-topic group, or the Building group. Where this is the case, it will either be stated expressly, or may be inferred by necessary implication
- Consequently, the statement "This group" applies to every OrangeFox group, unless excepted.

## 3. No discussions that are off-topic ("OT")
- The off-topic group exists for off-topic discussions. Every other group is subject to the "no off-topic discussions" rule
- This group is <u>only</u> for issues <u>directly connected with running official OrangeFox recovery releases</u>.
- This group does not allow <u>any</u> kind of off-topic discussions
- Do not ask, or discuss, or say anything about another recovery. Violating this may attract an <u>immediate ban</u>
- Do not ask, or discuss, or say anything about any unofficial OrangeFox build. Violating this may attract an <u>immediate ban</u>
- Do not ask anything about any ROM
- Do not ask about Magisk or any other Android application
- Do not ask about how to build OrangeFox. Go to the Recovery building and development group for that
- Do not talk about OrangeFox Beta releases. Go to the OrangeFox Beta testing group for that
- Do not discuss or ask anything about the OrangeFox app. Go to the app discussion group for that  
- Do not engage in racist, political and/or religious discussions, of any kind
- Do not start (or continue) any jokes. Go to the off-topic group for that kind of thing
- Do not post random emojis or their text equivalents (eg, lol, lmao, etc, etc). Go to the off-topic group for that kind of thing
- Do <u>NOT</u> respond, at all, to an off-topic message, especially if you are not an admin. If you do, you will be prolonging the off-topic message, and you will get a warning. The <u>ONLY</u> appropriate reaction of a non-admin to an off-topic message is to report it (using the "/report" command)
- Do not engage in banter. Go to the off-topic group for that kind of thing
- Do not engage in sarcasm. Go to the off-topic group for that kind of thing
- Do not ask why there is no maintainer (or whether there is a maintainer) for a device
- Do not ask why a particular feature (or device) is not supported; if it is not supported, then that is the end of the matter
- Do not give us your opinion on whether or not you think that our rules are appropriate, or "too strict", or whatever
- Do not ask about the differences between OrangeFox and another recovery, or about which is better between OrangeFox and another recovery
- Do not start (or continue) any discussion, of any kind, that is not <u>directly connected to the running of an official OrangeFox release</u>
- Violation of this rule, depending on the nature of the violation, may attract an automatic ban.

## 4. English only
- Only the English language is allowed here for communication. If you post anything in any other language, you <u>will definitely</u> receive a warning

## 5. No rudeness, profanity, swearing/cursing, obscenities, etc
- Mind your language, and be professional
- We are a global community, and things that may be acceptable in your country are not neccessarily acceptable elsewhere, so be sensitive in your choice of words
- Do not use profanity, sexually explicit language, vulgar words, expletives, or other offensive (in any language) words. There is absolutely no reason to swear, curse, or use expletives or profanities. And - do not use profanities, expletives, or other offensive language in your username or profile
- OrangeFox is used by people of all ages, including minors, and by people of all cultural backgrounds. Do not post nude or pornographic imagery of any kind (including the exposure of male or female genitalia and/or female breasts)
- Do not be rude or aggressive
- Do not make personal attacks
- Do not use racist/sexist, or any other discriminatory language. Discriminatory language of any sort will not be tolerated
- Breaking any of these sub-rules is liable to attract an <u>automatic ban from all our groups</u>

## 6. No tagging or pinging of admins or devs
- Do not tag or ping admins or devs unnecessarily (either directly, or indirectly). Admins have a life outside of telegram, and you must respect their time, which they are giving without charge
- No matter how urgent you may think your situation is, you <u>**must** absolutely resist</u> the urge to tag or ping an admin or dev, because, if you tag, you <u>**will definitely**</u> receive a warning. 
- If you tag/ping on Discord, you <u>**will definitely**</u> get at least a timeout (for a period between 1 day and 7 days); if you repeat the infringement, you may be kicked or banned
- Do not PM an admin or dev without first requesting (and securing) his/her permission in the open forum
- If you want to report an inappropriate post, use the `/report` command

## 7. No inappropriate attachments
- Do not attach any file, picture, video, or anything else, that has not been generated by an official OrangeFox release
- Do not attach a screenshot or picture of an OrangeFox screen without also attaching logs, and without also providing a full explanation of what is the problem, and how exactly you got to that point
- Do not attach OrangeFox logs without also providing a full explanation of what is the problem, and how exactly you got to that point

## 8. No Pirating/malware/merchandising/promotions
- Do not talk about (or post) pirated software or malware
- Do not or promote advertise any product, or service, or website, or any group/channel/chat, or anything else
- Do not recommend or suggest anything that is not an official OrangeFox release
- Breaking any of these sub-rules is liable to attract an automatic ban

## 9. No inappropriate actions
Since it is not possible to enumerate every possible misdemeanour, this is a general prohibition of anything that is inappropriate. It is a matter for admins to decide whether something is inappropriate. 

Here are some examples of inappropriate actions - this does not purport to be anything like an exhaustive list - so do not argue that (whatever an admin has decided is inappropriate) is not on this list:
- Trying to test the limits of the forum's controls
- Experimenting with (or trying to run) commands that are meant only for admins
- Flooding the group by running random commands, including running a command with a device name
- Fooling around with forum controls
- Allowing your anti-spam bot to spam the group
- Cross-posting of any sort. If you post the same (or similar) thing in different chats, you <u>will definitely</u> receive a warning. Make sure you have identified the correct chat for your post, and post it only once
- Trolling of any kind. We have a zero-tolerance policy towards trolling. If you engage in trolling of any kind, directly, or indirectly, or through your channel, you will be banned immediately
- No gaslighting or deception of any kind. We have a zero-tolerance policy towards deception and gaslighting. If you engage in gaslighting or deception of any kind, directly, or indirectly, or through your channel, you will be banned immediately
- Impersonating or attempting to impersonate anybody. If you are discovered, you <u>will definitely</u> be immediately banned from all our groups
- Wasting our time by asking questions without first <u>reading our wiki</u> - if you ask a question for which an answer is already provided in our wiki, you will receive a warning
- Wasting our time by asking questions without first <u>reading all pinned messages</u>
- Wasting our time by reporting problems without supplying <u>full details</u>, including the device, the ROM, the OrangeFox version, and full logs that demonstrate the problem
- Pushing and testing the patience of admins
- Posting links to external (ie, not OrangeFox) sites
- Giving inappropriate or misleading advice/recommendations to anybody
- Posting anything that is misleading
- Arguing or debating anything with anybody (ie, with admins, or anybody else)
- Sending unsolicited PMs to any member of an OrangeFox group
- Setting up channels from which rule-breaking messages are posted: this will attract an immediate ban from all our groups 
- Unnecessary tagging of any member of an OrangeFox group
- Creating "noise" of any kind in the group; this includes, but is not restricted to, nonsensical questions/statements; posting about problems without identifying your device and your OrangeFox version; posting without first searching the group for whether the same issue has already been covered; etc, etc

## 10. The decisions of admins are final, and must not be discussed or debated
- Do not argue with an admin over a decision that he/she has made; and do not debate the matter
- Do not ask or beg admins to remove a warning that they have issued to you or any other person
- Do not engage in any discussion or argument about whether an admin is justified in taking whatever action he/she has taken
