---
title: MIUI / Flyme OTA
description: 
published: true
date: 2022-07-26T13:51:07.344Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:00:23.148Z
---

## How do I setup incremental MIUI OTA in OrangeFox Recovery?
* Enabling incremental OTA updates in stock MIUI ROMs requires an intricate setup process, otherwise, it will not work.
* You cannot set up MIUI OTA in recovery mode for MIUI 13 (MIUI Android 12+) ROMs. These are updated directly by the MIUI updater.
* To set up MIUI OTA properly, take the following steps (this is mainly for creating a brand new MIUI installation):
1. Download the most up-to-date release of OrangeFox Recovery
2. Download a full MIUI ROM, and copy it to your phone (normally this should be copied to an external storage device)
3. If you don't already have OrangeFox installed, flash the OrangeFox zip with your current custom recovery (or follow the steps in the guides for installing from adb/fastboot)
4. Reboot into OrangeFox Recovery (this will have happened automatically if you flashed the OrangeFox zip)
5. Go to the OrangeFox settings, tap on "OTA & ROM" and turn on "Enable OTA". If you want to flash a stock MIUI 10/11/12 ROM that is based on Android 9.0 or higher, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the OTA settings, otherwise the incremental MIUI OTA will <u>most definitely not work</u> - the typical symptoms of this failure are that, when you tap on "Reboot now" after downloading the incremental OTA update, **nothing** will happen - or you just get an error if you try to download the incremental OTA update. If you get this problem, you will need to repeat this entire process (starting with this step #5), with the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings.
6. Go to the “Wipe” menu, and wipe cache/dalvik and data and then reboot OrangeFox (with FBE ROMs you may have decryption isues if you only wipe data - you may need to <i>format</i> data instead - but you could first try without wiping or formatting).
7. Flash your full ROM. After the ROM is flashed, OrangeFox will start the "OTA_BAK" process, which will backup your system (and sometimes vendor) and boot partitions into the /sdcard/Fox/OTA directory (this will be on your internal storage). <u>You must NOT delete any of the files in the /sdcard/Fox/OTA directory</u>. If you do, then incremental OTA updates will <u>most definitely</u> fail.
8. Reboot your phone, and check that the files in /sdcard/Fox/OTA folder are intact. 
9. If the files are intact, then you can start to use your phone normally. If the folder is empty, then follow the steps outlined in point 11 below.
10. If your data partition was <u>formatted</u> for the ROM installation, booting up the newly installed ROM for the first time will probably result in the ROM encrypting the device. This process will also normally delete the files backed up during the OTA_BAK process (in /sdcard/Fox/OTA). If this happens, you have to return to OrangeFox and dirty-flash the ROM again (ie, without wiping anything). This will allow the OTA_BAK process to run again. If you do not do this, the OTA settings page will simply tell you that you must flash the full ROM. Exactly the same considerations apply every time you format your data partition.
11. When MIUI notifies you that there is an update, download the update, using the MIUI updater, and allow it to reboot automatically to OrangeFox
12. OrangeFox will install the update automatically. Go and make a cup of tea, and some sandwiches, because this will take `several` minutes (at least 10 minutes). It is a `very` involved process, which has `3 stages` - restoring the stock backups from the Fox/OTA folder (about 1-3 minutes), flashing the OTA update itself (about 5-10 minutes), and then creating a new pristine backup in the Fox/OTA folder (about 1-3 minutes)
13. OrangeFox will reboot the phone automatically upon completion of the installation of the MIUI OTA update (unless you have ticked "Prevent auto reboot after installing OTA")
14. After this, you will not need to flash a full ROM any more - just follow the steps in #9 above
15. Enjoy

NOTES: 
* If you want to move to a completely different version/build of MIUI, then you will first need to clean-flash the full ROM of that MIUI build, as described above.
* If you formatted your data partition before (or after) flashing your ROM, there is a bit of a rigmarole if you want incremental OTA support. See point 10 above.
* This guide does not apply to Android 12 MIUI ROMs.

If you want incremental OTA support for your <u>existing</u> stock MIUI ROM (ie, not a new installation), then you must take the following steps after the stock MIUI ROM has booted for the first time after flashing it:
1. Reboot to OrangeFox
2. Make sure that the Enable OTA option is selected
3. If you have a MIUI 10/11/12 ROM that is based on Android 9.0 or higher, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings
4. Dirty-flash the original zip of your current MIUI ROM (so that it will set up the OTA support)
5. Reboot to system
6. Reconfigure your ROM, as may be required

## How do I set up block-based incremental OTA for custom ROMs in OrangeFox Recovery?
* One of the best known examples of block-based incremental OTA is MIUI OTA (see immediately above).
* This kind of OTA update is now being introduced in custom ROMs. The set up process is identical to the set up process for MIUI incremental OTA
* This feature is new (*and experimental*) for custom ROMs, and it not enabled for all devices - so you first need to ascertain that the device's OrangeFox maintainer has enabled it in his build of OrangeFox. If it has not been enabled, then there is nothing that you can do.
* If you have ascertained that the device's OrangeFox maintainer has enabled this feature, then read on!
* Enabling incremental block-based OTA updates requires an intricate setup process, otherwise, it will not work. To set it up properly, take the following steps:

1. Download the custom ROM, and copy it to your phone (normally this should be copied to an external storage device)
2. Go to the OrangeFox settings, tap on "OTA & ROM" and turn on "Enable OTA".
3. Flash your ROM. After the ROM is flashed, OrangeFox will start the "OTA_BAK" process, which will backup your freshly installed system (and sometimes vendor) images, and the boot partition, into the /sdcard/Fox/OTA/ directory (this will be on your internal storage). <u>You must NOT delete any of the files in the /sdcard/Fox/OTA directory</u>. If you do, then incremental OTA updates will <u>most definitely</u> fail.
4. Reboot your phone, and check that the files in /sdcard/Fox/OTA folder are intact. 
5. If the files are intact, then you can start to use your phone normally. If the folder is empty, then follow the steps outlined in point 6 below.
6. If your data partition was <u>formatted</u> for the ROM installation, booting up the newly installed ROM for the first time will probably result in the ROM encrypting the device. This process will also normally delete the files backed up during the OTA_BAK process (in /sdcard/Fox/OTA). If this happens, you have to return to OrangeFox and dirty-flash the ROM again (ie, without wiping anything). This will allow the OTA_BAK process to run again. If you do not do this, the OTA settings page will simply tell you that you must flash the full ROM.
7. When ROM notifies you that there is an update, download the update, using the ROM's updater app, and allow it to reboot automatically to OrangeFox (**Note**: do not try to flash a block-based OTA zip manually from the recovery - this will **not** work - it is necessary to do all incremental OTA updates via the ROM's updater app)
8. OrangeFox will install the OTA update automatically (this can take several minutes)
9. OrangeFox will normally reboot the phone automatically upon completion of the installation of the OTA update
10. After this, you should not need to flash a full ROM any more

## Returning to a MIUI ROM from a custom ROM
1. Bootup OrangeFox
2. Backup your internal storage (user files, eg, photos, etc) to an external device
3. Wipe cache, dalvik, and system
4. Format data
5. Reboot OrangeFox (just swipe to continue if it complains that no ROM is installed)
6. For a stock MIUI 10, MIUI 11, or MIUI 12 ROM that is based on Android 9.0 or higher, ensure that the "Disable DM-Verity" box is <u>unticked</u> in the MIUI OTA settings. If that box is ticked, then incremental MIUI OTA will not work
7. Flash the MIUI ROM
8. Bootup the ROM (during the bootup process, MIUI will probably encrypt the data partition, unless you ticked "Disable Forced Encryption" before flashing MIUI; the encryption process will also wipe your internal storage - see below for the implications of this)
9. Reboot to OrangeFox
10. Restore the backup of your internal storage

## Returning to a MIUI ROM from a custom ROM (old devices with unofficial/simulated Treble and that do not have a dedicated vendor partition)
+ "Simulated" Treble ROMs on many devices use MIUI’s “cust” partition for their vendor image. This process removes the Xiaomi proprietary files that are necessary for MIUI to run. These files must be restored, otherwise MIUI will not work properly.
+ For this purpose, the cleanest and easiest way to return to MIUI from a simulated Treble ROM is to flash a full fastboot MIUI ROM, using the Mi Flash tool.
+ If you did not create a backup of your MIUI cust partition to MicroSD or USB-OTG before installing a Treble ROM, then you must use the method just described above (or else you can try flashing a stock vendor image for your device (if there is one for your device there))
+ If you DID create a backup of your MIUI cust partition (in the newest versions of OrangeFox, this would be the “vendor” partition – it points to the same location as “cust”) to MicroSD or USB-OTG, then you can use OrangeFox to install MIUI when coming from a Treble ROM - but you have to take certain steps:
1. Backup your internal memory to an external device (eg, MicroSD, USB-OTG, or your PC). DO NOT SKIP THIS STEP!
2. Copy the latest stable MIUI ROM to your MicroSD or USB-OTG storage
3. Boot OrangeFox
4. Make sure that OrangeFox can read your MicroSD or USB-OTG storage, and can see the MIUI ROM that you copied there. This is the ROM that you will install in the steps below.
5. Select the “Wipe” menu and wipe everything - system, dalvik, cache, vendor, etc (except MicroSD/USB-OTG)
6. Format data (“format” - not “wipe”) - you will lose all the contents of your internal memory after doing this
7. Reboot OrangeFox - you will see a message saying “No OS installed …” - just swipe to reboot OrangeFox
8. Restore the backup of your MIUI cust partition (in the latest releases of OrangeFox, this will need to be restored to the "vendor" partition)
9. Flash your MIUI ROM
10. Reboot to system, and wait for a long time ...
11. When ready, restore your backup of your internal memory from your external storage device.

## Where are the settings for OTA?
+ Tap on *Menu*, and tap on the settings icon at the top right of the screen. You will see all the various settings, including "OTA & ROM".

## Can I delete the huge files in the Fox OTA folder?
+ No - unless you want your incremental OTA updates to fail completely.

