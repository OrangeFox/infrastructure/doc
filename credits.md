---
title: OrangeFox sponsors and credits
description: 
published: true
date: 2024-03-25T17:51:14.068Z
tags: 
editor: markdown
dateCreated: 2021-09-19T20:23:03.393Z
---

## Servers and VPSes

### [ua-hosting.company](https://ua-hosting.company)
![logo_sia_ua-hosting_(1).svg](/logo_sia_ua-hosting_(1).svg =530x)
Supports us by providing 2 VPSes in the Netherlands, which are used for mail server and other management workflows.


## Plans

### [Crowdin](https://crowdin.com)
![crowdin-logo-small-example@2x.png](/crowdin-logo-small-example@2x.png =530x)
Gave us an Enterprise account for better crowdsource translations for OrangeFox Recovery.

### [GitLab.com](https://gitlab.com)
![image_2021-09-22_20-18-01_(3).png](/image_2021-09-22_20-18-01_(3).png =530x)
GitLab graciously gives us a Gold plan for our GitLab project.

## Individuals
Unfortunally, due to OpenCollective's limitations here shown only people that dontates monthly.
Check out [our opencollective page](opencollective.com/orangefox) to see everyone who contributed to OrangeFox Recovery.
![](https://opencollective.com/orangefox/tiers/backer.svg?avatarHeight=65)
