---
title: Cборка OrangeFox
description: 
published: true
date: 2020-10-13T16:57:03.799Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:09:04.200Z
---

## Требования
+ ПК / Сервер
+ Мнго свободного места на вашем компьютере / сервере. Возможно, вам понадобится `не менее 50 ГБ свободного места` - но рекомендуется примерно `в два раза больше`
+ Правильное окружение Linux. Рекомендуется Ubuntu 18.04 или Linux Mint 19.3. ArchLinux работает нормально, но для этого нужны некоторые танцы с бубном.
+ TWRP дерево для устройства

## Первоначальная сборка OrangeFox
### 0. Подготовка среды сборки
``` bash
    cd ~
  sudo apt install git -y
  git clone https://github.com/akhilnarang/scripts
  cd scripts
  sudo bash setup/android_build_env.sh
  sudo bash setup/install_android_sdk.sh
```

### 1. Синхронизация источников OrangeFox
``` bash
    mkdir OrangeFox
  cd OrangeFox
  repo init -u https://gitlab.com/OrangeFox/Manifest.git -b fox_9.0
  repo sync -j8 --force-sync
```
+ ***fox_9.0** означает, что версия Android, на которой базируется манифест сборки OrangeFox, включает fox_7.1, fox_8.1, fox_9.0*

+ Подсказка 1: Используйте `repo init --depth=1 -u https://gitlab.com/OrangeFox/Manifest.git -b fox_9.0` для инициализации неполного клона для экономии дискового пространства

+ Подсказка 2: версия манифеста сборки и версия OrangeFox - `это совсем разные вещи`. Поэтому, `НЕ` спрашивайте про "исходный код R10". Если вы синхронизировались как показано выше, то у вас уже есть исходники для последних стабильных релизов OrangeFox (R9, R10, R11, R55, или ещё какая-нибудь).

### 2. Разместите деревья и ядро

Вы должны поместить деревья устройств, ядра в соответствующие директории. Например `device/xiaomi/mido`
``` bash
# Это пример команд
cd OrangeFox
mkdir -p device/xiaomi
cd device/xiaomi
git clone https://gitlab.com/OrangeFox/Devices/mido.git mido
```

### 3. Сборка
``` bash
    cd OrangeFox
    source build/envsetup.sh
  export ALLOW_MISSING_DEPENDENCIES=true 
  export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
  export LC_ALL="C"
  lunch omni_<device>-eng && mka recoveryimage
```

### 4. Заберите сборку OrangeFox
Если не было ошибок при компиляции, окончательный образ рекавери будет в out/target/product/<device>/OrangeFox-unofficial-<device>.img

## Помощь по сборке
+ Если вы хотите помочь/поддержать создание OrangeFox для вашего устройства, перейдите в нашу группу Телеграмм [OrangeFoxBuilding](https://t.me/OrangeFoxBuilding).

## Конфигурации
+ У OrangeFox много собственных конфигураций и переменных сборки (`build vars`), которые дают разработчикам точный контроль над возможностями, встроенными в рекавери, - [смотрите здесь](https://gitlab.com/OrangeFox/vendor/recovery/-/blob/master/orangefox_build_vars.txt), чтобы понять, какие вы должны применить.
+ Самый простой способ использовать переменные сборки — поместить их в vendorsetup.sh вашего дерева устройств. Подробный пример [смотрите здесь](https://gitlab.com/OrangeFox/device/lavender/-/blob/fox_9.0_Q/vendorsetup.sh). Примечание. Это всего лишь пример подходящих параметров для одного устройства. Разумеется, вам нужно будет изменить этот пример, чтобы использовать его для своего устройства.

## Как официальным сопровождающим

Читайте [здесь](/dev/maintainerships)
