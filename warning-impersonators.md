---
title: Impersonators warning
description: 
published: true
date: 2022-08-21T17:49:07.032Z
tags: 
editor: markdown
dateCreated: 2022-07-20T12:13:20.638Z
---

> Due to increasing rates of impersonation of OrangeFox Recovery staff by evil and malicious imposters you **MUST** be very careful before doing anything potentially dangerous with your device.
{.is-danger}

First of all, unlocking your device means that you lose all guarantees of security, and that you take **all responsibility** for the security and safety of your device onto yourself.

Secondly, do **NOT** just run any command that *ANYONE* advises you to run, without first **independently** verifying **for yourself** what exactly the command will do. Some malicious impersonators, with malevolent and perverted minds, may advise you (whether by PM or in Telegram chats) to run some commands, which, if you run them, will *hard-brick your device*. If you run those commands, you will only have yourself to blame for the consequences. OrangeFox staff will **not** answer any questions about such matters.

Thirdly, do **NOT** run any commands that include such things as "fastboot" and/or "erase". You are very liable to **permanently** trash your device if you do. If you run any such commands, you will only have yourself to blame for the consequences. OrangeFox staff will **not** answer any questions about such matters.

# Official OrangeFox Recovery contacts and resources
### Domains:
* https://orangefox.download
* https://orangefox.tech
* https://orangefox.info

### Telegram:

> Any chats/channels that are not listed here should be treated as untrusted and dangerous!
>
> **Most importantly** - DO NOT trust ANYONE who messaged you directly.
>
> OrangeFox Recovery staff are forbidden to message anyone directly!
{.is-danger}

#### Chats:
> **Real OrangeFox Recovery staff now use an anonymous admin feature** (aka messaging as the group itself). They always have an admin title like "Agent X"! (This is applies from 20.07.2022)
{.is-warning}
* https://t.me/OrangeFoxChat
* https://t.me/OrangeFoxBeta
* https://t.me/OrangeFoxApp

#### Channels:
* https://t.me/OrangeFoxNEWS
* https://t.me/OrangeFoxUpdates
* https://t.me/OrangeFoxBetaTracker
* https://t.me/OrangeFoxCommits


## OrangeFox mail
All emails sent from @orangefox.tech can be considered as safe, while the others are not.
