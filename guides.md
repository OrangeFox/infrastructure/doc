---
title: Guides and Frequently Asked Questions
description: 
published: true
date: 2020-10-13T16:55:22.890Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:22.697Z
---

- [Installing OrangeFox Recovery](installing_orangefox)
- [Basic questions](base)
- [Flashing](flashing)
- [Backups](backups)
- [MIUI / Flyme OTA](ota)
- [Encryption](encryption)
- [Reporting problems or bugs](report)
- [Others](others)
{.links-list}

