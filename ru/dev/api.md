---
title: OrangeFox Recovery API
description: 
published: true
date: 2020-10-13T16:56:58.774Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:08:59.645Z
---

> API v2 выпущен и готов к использованию начиная с 26 апреля 2020 API v1 будет остановлен. Чтобы проверить текущий статус API v1 см. [страница API v1](v1) 
> 
> {.is-success}

## Страницы версий API:
- [API v1](v1)
- [API v2](v2)
{.links-list}

## Пример использования OrangeFox API в проектах:
*имя | версия используемого API | язык*
- [OrangeFox Recovery Telegram бот (oBot) | v1 | Python](https://gitlab.com/OrangeFox/site/obot)
- [Сайт загрузок OrangeFox Recovery (dSite) | v2 | React](https://gitlab.com/OrangeFox/site/dsite)
{.links-list}
#### Сторонние
- [Модуль дружественного Telegram бота (FTG) | v1 | Python](https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/orangefox.py)
{.links-list}








