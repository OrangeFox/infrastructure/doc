---
title: Flashing
description: 
published: true
date: 2024-02-10T16:56:13.232Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:00:14.218Z
---

# Flashing ROMs
Flashing a ROM is usually a straightforward process. You can either do a "*clean*" flash, or a "*dirty*" flash. "Clean" flash is always recommended, but in some cases, like flashing ROM's update "dirty" flash will be fine.

> **Always make backups.**
Make a full backup of your current ROM - before performing any other operations in the recovery. At the very least, backup the /boot, /data, system_image (or system - if system_image is not available for backup) and vendor_image (or vendor - if vendor_image is not available for backup) partitions.
{.is-info}

> **Xiaomi ARB**
Xiaomi has broken ARB implemention which can brick your phone forever!
If your Xiaomi device has ARB, and you are on a MIUI ROM, then you had better tread very carefully if you want to change your MIUI ROM to a different MIUI ROM, or to a different version of the same MIUI ROM. Search on Google for "ARB", read and understand everything, and then decide whether you really need to proceed.
{.is-warning}


### "Clean" flashing (Strongly recommended)
1. Bootup OrangeFox Recovery
2. Select "Wipe", and tick data (only if your device is not encrypted, or if it is encrypted with `FDE`). Then tick cache, and tick dalvik. Unless there is a <u>very specific reason</u> which requires it, do *<u>NOT</u>* wipe system or vendor - wiping them manually might *end in tears* (in any case, if your device has dynamic partitions, then you will not find system or vendor in the "wipe" list - which is a <u>very good thing</u> indeed). If you are changing ROMs on a device that is encrypted with `FBE`, then you must `format` data before AND after flashing the new ROM
3. Swipe to wipe (this will restore the installed ROM to a known state, and will remove apps/settings that might be incompatible with the ROM that you wish to install)
4. Select the ROM that you want to flash
5. Swipe to flash
6. Reboot OrangeFox recovery - before doing anything else - so that any changes to partitions/filesystems done by flashing the new ROM will take full effect
7. Flash whatever else you might want to flash (eg, GAPPs)
8. Format the data partition
9. Reboot your device
10. Wait for a long time while the new ROM sets itself up (go and make a cup of tea!)
11. Enjoy

NOTES: 
A. In most cases you need to <u>format</u> (not just wipe) the data partition. In such cases, you should format the data patition, flash the ROM, reboot OrangeFox, and format the data partition again, <u>before</u> rebooting to system.
B. If you are moving from a ROM that uses a different encryption protocol (eg, from FDE ROM to FBE ROM or vice-versa), then `you must format the data partition`.
C. If you are moving from a stock ROM to a custom ROM (or vice versa), or if you are changing from one ROM to another ROM, then `you must format the data partition`.
D. If you boot the newly flashed ROM, and it gets stuck forever on the Google/Android logo, or just dumps you at fastboot or straight back to OrangeFox, this might be a sign that you need to `format` the data partition.


### "Dirty" flashing (NOT recommended)
Please not that dirty flashing a ROM is not recommended. It can cause all kinds of random problems.

1. Bootup OrangeFox Recovery
2. Choose the ROM that you want to flash
3. Swipe to flash
4. Wipe cache and dalvik
5. Reboot OrangeFox
6. Flash whatever else you might want to flash (eg, GAPPs, etc)
7. Reboot your device (if it does not bootloop)
8. Enjoy (if the device actually manages to boot, and you do not get random errors)

## A/B devices
+ Most of the information on this page relates to A-only devices. A/B devices require some different considerations, which are not covered here.
+ If you have an A/B device, it is best to refer to the specific flashing instructions given by your ROM's developers, instead of the general information supplied here.
+ With A/B devices, it can be very helpful to format the data partition - both before - and after - flashing a ROM.
+ With respect to flashing ROMs, Flashing a ROM will, in almost all cases, automatically replace OrangeFox with the ROM's own recovery. This behaviour `cannot be changed`. That is how things were designed to operate on A/B devices.
+ So, <i>unless the maintainer of the ROM that you are about to flash has embedded OrangeFox within the ROM itself (and has announced this fact)</i>, you must, <u>immediately</u> after flashing a ROM (including an OTA update), flash the OrangeFox zip again, then reboot OrangeFox, <u>before</u> doing anything else. If you do not follow these steps, then you can expect problems (including the installed OrangeFox having been overwritten by the ROM installation).
+ For Virtual A/B and some other devices with the supporting feature, you can also restore OrangeFox after a ROM flash in other ways - eg, choose "<i>Flash current OrangeFox</i>" from the main menu after flashing the ROM, or, better still, before flashing the ROM, tick the box that says "<i>Reflash OrangeFox after flashing a ROM</i>" when you select the ROM zip file that you want to flash.

## I want to "clean" flash a new ROM. What do I need to wipe before flashing?
+ At the very least, you should wipe *Dalvik/Art Cache, Cache, and Data*.  
+ Some ROM developers recommend also wiping System. Unless there is a **very specific reason** why you **really need** to do this, we do **<u>not</u>** recommend wiping System or Vendor.
+ Note that, on some ROMs with FBE encryption, wiping data and rebooting the recovery may leave your internal storage inaccessible (you will just see gibberish characters instead of filenames, and you will not be able to read or write to your internal SD). Normally you should be able to just go ahead and flash your ROM. But if trying to boot to system (after flashing the ROM) just reboots straight back to recovery, then you will need to format the data partition.

## I want to flash something. How do I do select the file that I want to flash?
Tap on "*Files*", and navigate to the file that you want to flash. Tap on it, and you will see
the options for *flashing*. If you long-press on a file name, you will see a list of all the 
operations that you can perform on that file.


## I get an "error 7" when trying to flash (whatever)
+ The dreaded "`error 7`" can be caused by any number of things. Whatever is the specific cause in your case will be clearly displayed in red, and it will also be in the recovery log file. 
+ Usually, the proximate cause is a check that the author of your zip installer's updater-script has added - whether it is for a `specific firmware` version, or some other specific feature, or you are simply trying to flash a zip meant for another device onto a wrong device. There will be an "`assert`" statement, which means that if the check fails, an error will be triggered. Other possible causes include trying to flash a zip file that is corrupted, and trying to flash something meant for one device onto another device. 
+ Read very carefully what is on your screen and in the recovery log file, and address it. 
+ You may also want to [run a Google search](https://www.google.com/search?source=hp&ei=bt2nXp7cDJCPlwSrn6LgAQ&q=TWRP+error+7&oq=TWRP+error+7)
+ Do *not* ask for support, simply stating that you have got an error 7. You *must* provide details as to what is stated to be the cause, and *you must attach a copy of the recovery log*. If you do not do these, any request for support will simply amount to *noise*, and we will just ignore it.

## Should I flash a DFE zip after flashing a ROM?
+ Under normal circumstances, there is no reason why you need to do that.
+ However, if the developer of your ROM advises you to do so, then you should always follow the advice of your ROM's developer. But know that `you are on your own` in such cases - so please do not come to tell us that something went wrong after flashing a DFE.

## Should I tick "Disable DM-Verity" before flashing my ROM?
+ This feature is now deprecated, and will disappear totally from new OrangeFox releases.
+ For older releases running on Android 10 and higher ROMs, you normally should <u>not</u> tick that box. But note the following below:
+ 1. Some legacy ROMs will not boot if you disable DM-Verity
+ 2. Some legacy ROMs will not boot if you <u>don't</u> disable DM-Verity
+ 3. How do you know the difference? Ask in your legacy ROM's forums - or by trial and error - ie, test for yourself to see which is which (and you may need to do this for different ROMs even on the same device).

## Should I tick "Disable Forced-Encryption" before flashing my ROM?
+ This feature is now deprecated, and will disappear totally from new OrangeFox releases.
+ For older releases running on legacy ROMs, normally, you should <u>keep your device encrypted</u>, because <u>that is the best way to secure your personal data if you phone gets lost or stolen</u>. If you care at all about the security of your personal data, you should <u>not</u> tick that box.

## Should I wipe System and/or Vendor before flashing my ROM?
+ No! No! No! Do not `EVER` do this!
+ Is that clear enough? 
+ Please do <u>NOT</u> come to our groups or forums to ask about this. The answer will <u>ALWAYS</u> be "No".
+ You should <u>NEVER</u> have to wipe system or vendor manually. 
+ If the instructions for flashing your ROM recommend doing this, you should <i>ignore the recommendation</i>.
+ If you really insist on trying to wipe these partitions, then <u>you are on your own</u>.
+ If your device has <u>dynamic partitions</u>, then you cannot wipe system or vendor anyway, even if you wanted to (unless you do it via fastboot - and if you attempt this, <u>you are on your own</u>).
+ A "clean flash" simply means: Flash the ROM + Wipe cache & dalvik + Wipe or <u>format</u> data (as may be required).

## How does "format data" differ from "wipe data"?
+ <u>Wiping</u> data simply deletes all the user apps and user data. This leaves intact the contents of the internal storage (eg, your pictures, movies, personal files, etc). It also leaves encryption intact - unless your device has `FDE` (see below).
+ <u>Formatting</u> data (you would have to type "yes" to agree to the format) will delete all user apps, all user data, all the contents of the internal storage, and will also remove encryption. After this process, the device is unencrypted, and all the contents of the internal storage (eg, your pictures, movies, personal files, etc) will be gone. So, if there are things in your internal storage that you do not wish to lose, then you <u>must</u> first make a backup of your internal storage (to an external storage medium) <u>before</u> trying to format data.
+ If the device has a <i>full-disk encryption</i> (or `FDE`) then <u>wiping</u> data + caches is effectively equivalent to a "factory reset". This should be sufficient in most cases for a "clean flash".
+ If the device has a <i>file-based encryption</i> (or `FBE`) just wiping data will leave the encryption in a messy state, and may lead to problems. This is really <u>not</u> equivalent to a "clean flash", and should be avoided. 
+ For `FBE` devices, you should <u>format</u> data if you want a clean flash (and most certainly if you are changing your ROM). This should normally be done <u>after</u> flashing the ROM, and then you should reboot to system immediately afterwards (note that, for A/B devices, you should format data both before and after flashing the ROM).
+ If you do not <u>format</u> data when changing ROMs on an `FBE` device, you <u>WILL</u> definitely have problems. This may range from being stuck forever at the boot logo, to being always unceremoniously dumped back to recovery mode or to the bootloader. The solution is to boot the recovery, and format the data partition (which you really should have done in the first place, thereby avoiding all the problems).
+ Most persistent issues with encryption require <u>formatting</u> data.

## How can I tell whether my device has dynamic partitions?
+ If your device has a "Super" partition, or if it has "logical" flags for system, vendor, and product, in its fstab file, then it has dynamic partitions
+ If your device shipped from the factory with Android 10 or higher, then it almost certainly has dynamic partitions
+ Connect the device to a PC with a USB cable, and run the command: `adb shell getprop ro.boot.dynamic_partitions` (if the output is "true", then it has dynamic partitions).

## How can I tell whether my device is an "A-only" or "A/B" device?
+ Connect the device to a PC with a USB cable, and run the command: `adb shell getprop ro.boot.slot_suffix`
+ If the device is an A/B device, then you will see what is the current slot being used (eg, "_a" or "_b")
+ If device is an A-only device, then the command will produce no output

# Flashing images
+ In order to flash an image file, tap on the image file, and you should see a list of the possible partitions that the image can be flashed to. Select `the correct partition` for the image, and swipe to install the image.
+ If tapping on the image file does not automatically present a list of possible target partitions, then `long-press` the image file, then select "Open as...", then select "Flashable image". This should present the list of the possible partitions that the image can be flashed to. Select `the correct partition` for the image, and swipe to install the image.
