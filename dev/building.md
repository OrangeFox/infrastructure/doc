---
title: Building OrangeFox
description: 
published: true
date: 2024-04-07T20:12:31.968Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:35.867Z
---

# Requirements
+ Plenty of free disk space on your PC / Server. You would probably need a **minimum of `60GB` free space** - but you really should provide about **double** that - all this space is needed for the build system itself (about 35GB-45GB, depending on the manifest version), for the device trees, for the actual build (between 8GB to 12GB for each device), and for ccache's cache (if you use ccache).
+ Plenty of RAM - if you are building with the 11.0 manifest, you need a `minimum` of 16GB RAM (20GB would be much better). If you are building with the 12.1 manifest, you need a `minimum` of 20GB RAM (24GB would be much better).
+ A proper and full `Linux` <u>development</u> system (note: "<i>Linux</i>" - <u>NOT</u> "Windows"). 
+ A Debian-based Linux distro (eg, Ubuntu 20.04, Linux Mint 21.1) with a full development enviroment is recommended (including java, gcc, etc). If you use any other Linux distro, you are on your own.
+ Python 2.x (to find out your python version, run `"python -V"`; if it is not v2.x, then you need to symlink python to whatever python 2 version is installed on your system) - only if you're using older OrangeFox sources. 
+ OrangeFox/TWRP device tree for the device
+ Up-to-date `bash` shell (note: "<i>bash</i>", <u>NOT</u> "zsh", or "tcsh", or "ksh", or "csh", or "dash", or any other shell)
+ NOTE: do `NOT` build as root

# Initial build of OrangeFox
## 0. Prepare the build environment (Debian-based Linux distros)
``` bash
	cd ~
  sudo apt install git aria2 -y
  git clone https://gitlab.com/OrangeFox/misc/scripts
  cd scripts
  sudo bash setup/android_build_env.sh
  sudo bash setup/install_android_sdk.sh
```

## 1. Sync OrangeFox sources and minimal manifest
### Using our sync shell script from our "sync" repository; do NOT run this as root. The example below uses a script to sync the fox_12.1 branch.
NOTES: 
+ This method requires familiarity with linux shell scripts.
+ If you want to build for Android 12/Android 13 ROMs, sync the fox_12.1 branch. If you want to build for Android 11 ROMs, sync the fox_11.0 branch.
``` bash
mkdir ~/OrangeFox_sync
cd ~/OrangeFox_sync
git clone https://gitlab.com/OrangeFox/sync.git # (or, using ssh, "git clone git@gitlab.com:OrangeFox/sync.git")
cd ~/OrangeFox_sync/sync/
./orangefox_sync.sh --branch 12.1 --path ~/fox_12.1
```
 
+ Tip: The version number of the build manifest is <u>a very different thing</u> from the OrangeFox release version numbers. If you have synced as shown above, then you already have the sources for the latest OrangeFox Stable releases for whichever branch you have synced.

NOTES:
+ The process of syncing the sources will take a very long time. Depending on the speed of your internet connection, and on which method of syncing you use, it can take <u>hours</u>.
+ After you finish building, your build may have problems with decryption. If this happens, there is little that we can do to help you. You will need to work on your device tree.

## 2. Place device trees and kernel

You have to place your device trees, kernels in the proper locations.
For example `device/xiaomi/lavender`
``` bash
# These are example commands
cd ~/fox_12.1 # (or whichever directory hosts the synced manifest)
git clone https://gitlab.com/OrangeFox/device/lavender.git device/xiaomi/lavender
```

## 3. Build it
``` bash
	cd ~/OrangeFox # (or whichever directory has the synced manifest)
  /bin/bash # if your Linux shell isn't bash
  export ALLOW_MISSING_DEPENDENCIES=true 
  export FOX_BUILD_DEVICE=<device>
  export LC_ALL="C"

# for all brances
  source build/envsetup.sh

# for branches lower than 11.0
  lunch omni_<device>-eng && mka recoveryimage

# for branches lower than 11.0, with A/B partitioning
  lunch omni_<device>-eng && mka bootimage
  
# for the 11.0 (or higher) branch
  lunch twrp_<device>-eng && mka adbd recoveryimage

# for the 11.0 (or higher) branch, with A/B partitioning
  lunch twrp_<device>-eng && mka adbd bootimage

# for the 12.1 (or higher) branch, vendor_boot-as-recovery builds [this is highly experimental and unsupported!]
  lunch twrp_<device>-eng && mka adbd vendorbootimage

```
### Building tips
+ A. If you get any errors relating to anything with a ".py" extension or anything containing "py2",etc, then it means <u>you need to install Python 2.x</u>. Run "<u>python --version</u>" to see which version is being used as default - and, if it is not Python 2.7.x (eg, if it is Python 3.x) then you will have issues, and you will need to install v2.x
+ B. Indeed, you should always make sure that your default python for building is python 2.x

+ C. If you get build errors relating to "ui.xml for TW_THEME: portrait_hdpi", "Set TARGET_SCREEN_WIDTH and TARGET_SCREEN_HEIGHT to automatically select an appropriate theme", or "set TW_THEME to one of the following", etc, then it is possible that the bootable/recovery/gui/theme/ directory has not been properly synced. In that case, you might need to run a command like: `git clone https://gitlab.com/OrangeFox/misc/theme.git bootable/recovery/gui/theme` from the manifest root directory. 

+ D. If the device that you are building for is <u>not</u> a Xiaomi MIUI device, and/or you don't care about block-based incremental OTA updates, then you should consider adding this to your build process: 
`export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1`
or
`export FOX_VANILLA_BUILD=1`

+ E. If the device for which you are building OrangeFox is an A-only Xiaomi MIUI device, then you  probably want to add this to your build process, to prevent the MIUI recovery from overwriting OrangeFox when you boot into the MIUI ROM:
`export OF_PATCH_AVB20=1`

+ F. If you are building for an A/B device (you must be sure of this!), then you should add this to your build process:
`export FOX_AB_DEVICE=1`

+ G. If you are building with the `fox_11.0` (or higher) branch and your device is a recent `Virtual A/B ("VAB")` device, then you should add this to your build process if you are getting problems with flashing the OrangeFox zip installer itself, or with things like changing the splash :
`export FOX_VIRTUAL_AB_DEVICE=1`

+ H. If you want your subsequent builds to be faster, then use <i>ccache</i> - eg
`export USE_CCACHE=1`
`export CCACHE_EXEC=/usr/bin/ccache`
`ccache -M 50G` # this sets aside 50GB disk space for the cache

+ I. If you are using a prebuilt kernel, and you start getting a "NO KERNEL CONFIG" error, try adding `export OF_FORCE_PREBUILT_KERNEL=1` to your build process (eg, in vendorsetup.sh)

+ J. If your OrangeFox build variables are not being processed, ensure that your Linux shell for building is `bash`, and that you have run `export FOX_BUILD_DEVICE=<device>` before building.

+ K. If your build is getting stuck on the logo (indicating a decryption problem), you should consider providing a default version number for the keymaster services, using the `OF_DEFAULT_KEYMASTER_VERSION` variable (in fact, it is <u>strongly recommended</u> to always do this anyway). This needs to specify the version number of the keymaster services that you have provided in your device tree (ensure that this is correct) - eg, for keymaster version 4.1:
`export OF_DEFAULT_KEYMASTER_VERSION=4.1`

+ L. If you enable `TW_NO_SCREEN_BLANK` in your device tree, then make sure that you give a correct value for `TW_MAX_BRIGHTNESS` - othewise, a default value (1023) will be used - and this may not be what you really want.

## 4. Take the OrangeFox build
If there were no errors during compilation, the final recovery image would be present in out/target/product/[device]/OrangeFox-unofficial-[device].img

## 5. Help
+ If you want help/support with respect to building OrangeFox for your device, go to our [OrangeFox Recovery](https://discord.gg/qbVMJdJbXF) discord server.
+ Make sure that you strictly follow [the rules of our telegram and discord groups](https://wiki.orangefox.tech/en/telegram_rules), otherwise you may be issued with warnings - or bans.
+ If you have problems with build errors, or with booting up a successful build, or with things not working properly or not looking right, then you `must` provide the following:
a) a link (on <u>gitlab</u> or <u>github</u>) to <u>your device tree</u> - this `must` be the <u>exact</u> version that you used to build the OrangeFox build that you are reporting problems for; 
b) a link (on a site like https://del.dog or https://pastebin.com) to a <u>full</u> log of your entire build process - do <u>NOT</u> just post screenshots or excerpts from the build logs.

If you do not provide <u>both of these</u>, then your request for help will either be <u>ignored</u>, or it will be <u>deleted</u>.

+ If you truly want assistance with debugging your problematic build, then
a) <u>You must provide a very detailed account</u> of what <u>exactly</u> you tried, and what <u>exactly</u> happened when you tried it. 
b) You must also provide a full account of the OrangeFox build variables that you used; 
c) You should also try to get a `logcat` (if booting is not successful), or, 
d) If booting is successful but the recovery is not behaving correctly, then you must also post the recovery logs 
+ Menu->Copy Log to SD", including the kernel log
or
+ adb pull /tmp/recovery.log
+ adb pull /tmp/logcat.log

These logs must be posted to del.dog or pastebin.com or a similar site).

+ Do <u>NOT</u> post silly messages like "it doesn't boot", or "it doesn't work", or "it fails", or "why this (or that) is happening", or anything along those lines. Such messages are <u>totally useless</u>, because they do not tell anybody what <u>exactly</u> is going on, or what exactly happens when you try to boot, or how exactly you are trying to boot, or anything meaningful that anyone could use to try to help you. We may simply ignore (or delete) such messages. 

+ If your build does not succeed in booting at all, then;
a) You must first ensure that you have <u>correctly followed all the building instructions</u> (especially those relating to `build variables`). 
b) Secondly, you must also ensure that the `kernel` you are using is not problematic. 
c) Thirdly, you must satisfy yourself that your `device tree` is correct.

+ If errors occur during the build process, then, you should, in addition to providing a link to your device tree, 
a) Tell us which command you used to do the build (eg, "mka bootimage", etc);
b) Capture a <u>full</u> log of the build process (not just extracts of where you think the error occurs) and post it to a site like pastebin.com  or a similar site, and 
c) Post a link to the build log. 
d) You can capture a log of the build process by running a command like: `mka recoveryimage | tee build.log`

## 6. Configs
+ OrangeFox has many of its own configurations and build variables ("build vars"), which give developers fine control over the features that are built into the recovery - [see this](https://gitlab.com/OrangeFox/vendor/recovery/-/blob/fox_12.1/orangefox_build_vars.txt) (for fox_12.1 builds only) [or this](https://gitlab.com/OrangeFox/vendor/recovery/-/blob/fox_11.0/orangefox_build_vars.txt)  (for fox_11.0 builds) to understand which you should to enable.
+ You should put the OrangeFox-specific build vars in a shell script, or in `vendorsetup.sh` in <u>your own device tree</u>. For a detailed example, [have a look at this](https://gitlab.com/OrangeFox/device/vayu/-/blob/fox_12.1/vendorsetup.sh). Note - this is just an example of appropriate settings for one device. Obviously, you will need to amend this example in order to tailor it for your own device.
+ Do <u>NOT</u> put OrangeFox-specific build variables that start with "FOX_" in BoardConfig.mk - they will not be processed properly, or at all, if they are in any ".mk" file - they must be in a script, or exported manually at the command line.

### Q. How can I tell whether my device has dynamic partitions?
+ If your device has a "Super" partition, or if it has "logical" flags for system, vendor, and product, in its fstab file, then it has dynamic partitions
+ You can connect the  device to a PC with a USB cable, open a command window, and run this command: 
`adb shell getprop ro.boot.dynamic_partitions`
If the device has dynamic partitions, the output of the command will show "true" 
+ If your device shipped from the factory with Android 10 or higher, then it almost certainly has dynamic partitions

### Q. How can I tell whether my device is an "A-only" or "A/B" device?
+ Connect the device to a PC with a USB cable, and run the command: `adb shell getprop ro.boot.slot_suffix`
+ If the device is an A/B device, then you will see what is the current slot being used (eg, "[_a]" or "[_b]")
+ If device is an A-only device, then the command will produce no output
+ In bootloader/fastbootd mode, the command to run is: `fastboot getvar current-slot`

## 7. Updating the sources and/or build system
+ If you cloned the sources using the `orangefox_sync.sh` script, then follow the update instructions provided on [the OrangeFox sync page](https://gitlab.com/OrangeFox/sync)


## 8. The road to becoming an official maintainer


Read [here](/dev/maintainerships)
