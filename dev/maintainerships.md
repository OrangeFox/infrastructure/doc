---
title: Road to be an official maintainer
description: 
published: true
date: 2024-01-02T11:23:45.428Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:49.530Z
---

## Requirements 
+ Telegram and GitLab account
+ Account on our Discord server
+ Familiarity and compliance with all our Telegram/Discord rules
+ Familiarity with the contents of the OrangeFox wiki
+ Experience of using GNU/Linux or macOS for some time
+ Great <u>attention to detail</u>, evidenced, among other things, by the high quality of your device tree (it is not our role to debug your device tree for you), and by your ability to abide by official guidelines
+ Having the device in hand
+ Having a fully working unofficial build
+ Having an unofficial release of your unofficial build, with an XDA thread for it, which you are maintaining, and which shows that the builds have been well-received by the device's XDA community; the XDA thread must have been up and running for at least 4 weeks
+ Having your complete device tree on gitlab
+ Be ready to support your own builds, community part, etc
+ Not to be afraid of testing, **much testing**:
Testing new releases is the most important part of maintaining OrangeFox Recovery. Every new release should be <u>fully tested</u> and guaranteed to be working
+ You should not have a negative reputation in the Android community
+ You should not be rude
+ Hands and brains
+ Responsibility, maturity, and patience
+ Commitment and dedication (we neither need nor want "fly by night" maintainers, who are "here today, gone tomorrow")
+ Full compliance with the maintainers' guidelines (see "Important Notes", below) right from the start

## Steps
For first you should properly test your unofficial build and fix all known bugs, and establish that it passes our test suite:
### Test suite
#### Basic functionality
1. Installing .zip files work
2. Installing .img files work
3. Backup works
4. Restore works
5. The external SD card / OTG (if there is one) can be read
6. Backup to external SD card / OTG works
7. MIUI OTA works (on supported devices)
9. Built-in features work normally
9. Settings work and are kept after a reboot
10. OrangeFox Recovery can decrypt encrypted data partition without asking for a password if no lockscreen password has been set in the ROM

#### Advanced functionality
11. Recovery password protection works
12. Flashlight works (on supported devices)
13. Changing themes and splash work and are kept after reboot (on supported devices)

You should fix as many issues as you find. But if you can't fix some issues it won't be a reason for refusing your request for maintainership - but every release you must clearly document all known bugs.

Some advanced functionality is not critical. You can easily disable features that are not working via special build vars. 

----
### IMPORTANT NOTES:
+ All maintainers and prospective maintainers <u>must</u> follow the [maintainers' guidelines](/dev/maintainers_guidelines). 
+ These include base rules about fstab and more. 
+ You should `read the guidelines` before going any further. They explain our fundamental ground rules, and you need to know that you can comply with `all` of them `before applying` to become a maintainer. Otherwise, you will end up wasting your time, and our time.

----
## Applying to become an official maintainer
+ If you meet the requirements listed above, you can message in `#apply-for-maintainer` on our Discord server to apply for official maintainership. Here is an invitation link - https://discord.com/invite/qbVMJdJbXF

### When applying to become an official maintainer, you *must* provide <u>all</u> of the following:
1. A link to your device tree on gitlab.
2. A link to the XDA thread for your unofficial releases.
3. The OrangeFox <u>recovery logs</u> that show the success of your tests, as proof that your builds pass the test suite.
4. The <u>install.log</u> and <u>post-install.log</u> files that were generated when you flashed the OrangeFox zip. 
+ It is not sufficient simply to <u>tell us</u> that it all works. We `require hard evidence`, in the form of the `logs` that have been specified.
+ If you do not provide **all** of the above, your request to become a maintainer may simply <u>be ignored, or deleted</u>.

### After being accepted as an official maintainer
+ Your next step will be to ask for GitLab permissions in order to push your device trees to the OrangeFox gitlab repo. 
+ Then you will be allowed to push a Beta release. 
+ Only after positive and successful beta testing, lasting at least 14 days, will you be able to make a Stable release.

----
## How will builds be released?
We have our own maintainers panel website which will do all the hard work for you.