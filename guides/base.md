---
title: Basic questions
description: 
published: true
date: 2024-02-09T15:48:30.505Z
tags: 
editor: markdown
dateCreated: 2020-06-26T06:00:04.571Z
---

## How do I configure the OrangeFox Recovery?
Tap on *Menu*, and tap on the settings icon at the top right of the screen. You will see all the various settings.

## How do I donate to the OrangeFox Recovery project?
+ You can donate to us on [our Open Collective page](https://opencollective.com/orangefox)

## Why is there no OrangeFox for my device?
Really, you should <u>not</u> ask this question at all. But if you insist on asking, the answer is that there are many possible reasons;
1. We don't have the device (in which case, you are welcome to donate a device, or donate the money to buy it, or build the recovery yourself);
1. Nobody is interested in maintaining OrangeFox for the device (in which case, you are welcome to build for yourself, and, if successful, you can request to become an official maintainer, and we will consider the request in due course);
1. A working build has proved impossible, for any number of reasons (in which case, nothing more can be done - but you are welcome to try to build it for yourself);
1. The device was supported before, but the maintainer no longer wishes to maintain it (in which case, you are welcome to build it for yourself, and request to become a maintainer for it);
1. Other reasons;

The "long and short of it" is that if it doesn't exist, there is a good reason why it doesn't exist. If you think that you really "need" or "want" it, then please feel free to build it yourself.

## Will there ever be OrangeFox for my device?
+ You have simply asked the previous question in a different way. The answer is exactly the same as the answers just given above.

## Will there be OrangeFox for Android version [ whatever ]?
+ Perhaps - if you write the necessary code yourself.

## Will there be a [ new/updated/improved/whatever ] release of OrangeFox for my device?
+ This (asking about ETAs or possible future releases) is a `forbidden` question. If you ask this in our groups, you <u>will certainly</u> get a warning.
+ There will be another release when/if there is one. And, if there isn't ever another release for your device, then that's life. Feel free to build one for yourself.

## Why is my device marked as "Not maintained"?
+ The device was supported before, but the maintainer no longer wishes to maintain it, whether because he no longer owns the device, or for any other reason (in which case, you are welcome to build it for yourself, and request to become a maintainer for it).

## Why is [such and such] feature not supported?
+ Because we have not implemented it. But if that feature is really important to you, then please feel free to implement it yourself in your own unofficial build.

## Will [such and such] feature be supported?
+ Maybe, and maybe not. Probably not. But you are welcome to implement it yourself in your own unofficial build.

## How do I format data?
+ Go to *Menu -> Manage Partitions -> Data -> Format Data* and then type "yes". 
+ When you have finished formatting your data, reboot OrangeFox *before* doing anything else.
+ NOTE: formatting the data partition `will also delete the contents of your internal storage` - so you had better backup your personal files (eg, photos, music, etc) before formatting. If you do not take a backup of your files, there will be no way of restoring them after formatting.

## When I tried to format my data partition, I get errors
Try any of these work-arounds: 
+ Wipe the data partition, then reboot OrangeFox, then try formatting the data partition again; or
+ Boot to Android, remove all lockscreen protection, then reboot OrangeFox, then try formatting the data partition again; or
+ Bypass the password prompt when booting to recovery, so that data is not decrypted, then try formatting the data partition again; or
+ If all attempts to format the data partition in recovery mode do not succeed, then your best option (at least, for devices with Snapdragon chipsets) is to connect your device to your PC via a USB cable, reboot to bootloader mode, and then run the command: ``fastboot -w`` 
(this command will wipe the data partition; after it has finished, boot OrangeFox with the power + volume up key combination, and then format the data partition again from OrangeFox).

## How do I do a "clean" flash?
+ Wipe cache, and davlik. 
+ Then format data. 
+ Then flash your ROM. 
+ Then reboot OrangeFox. 
+ Then format data again. 
+ Then reboot to system.

## If I try to boot my ROM, I just get returned to OrangeFox - what do I do?
+ Look at the log screen in OrangeFox, and see what messages are there. Follow any recommendations that you see there.
+ Get the logs via "adb" (see below) and look for things like "`Android Rescue Party trigger`" or any other message that may explain why you have just been dumped back to OrangeFox, and follow the instructions given.

## If I try to boot into OrangeFox, it automatically boots into system. How can I solve this problem?
+ Assuming that other people can boot OrangeFox properly (which will definitely be the case, if you are using an official release) and that you are trying to boot into OrangeFox the correct way, then the problem is `on your device` - either your ROM, or your firmware version, or the specific version of your device. So you will need to supply `full details` about your situation.
+ Supply all the information prescribed in the [Reporting problems page](https://wiki.orangefox.tech/en/guides/report).

## How can I do a "factory reset" (restore to default settings)?
Tap on "*Wipe*", tick *Dalvik/Art Cache, Cache, Data*, and swipe to wipe. 

## How can I flash an image file?
1. Long-press the image file
1. Tap on "Open as ...", 
1. Select "Flashable image"
1. Select the partition to flash the image to
1. Swipe to flash

## Can I run "fastboot boot" to boot an OrangeFox image without flashing it?
+ This depends entirely on the device, the ROM, and the recovery variant. You should read the release notes of your official OrangeFox build for further information.
+ If your device is an A/B device, or is a recent device (ie, 2021 and afterwards) then you may be able to do so. But it depends on the device.
+ If you are running a virtual A/B device with a "vendor_boot" recovery, then it will NOT work. Do NOT even try it.
+ If your device is an A/B device with a dedicated recovery partition, then you must NEVER try to use "fastboot boot". It will NOT work, and you may get bricked device.
+ If your device, or its bootloader, or your ROM, does not support this command, then you are stuck.
+ In any case, this is not an OrangeFox issue.

## I flashed the Magisk addon but the ROM isn't rooted
+ Just flashing the Magisk addon in recovery mode is not sufficient for recent versions of Magisk (from 25.x onwards).
+ You need to reboot to system (don't wipe anything before rebooting), and then install the Magisk app's apk (same version as the addon that was flashed in recovery).
+ After this, the Magisk app will probably need to download and install some things. You need to allow it to do so. After it invites you to reboot the system, do so, and cross your fingers. It may work, or you may get a trashed ROM.
+ So. `Here is the best possible advice`, which you <u>ignore at your own peril</u>. 
+ `BEFORE` flashing Magisk of any kind, you `should` make backups of your `boot and data` partitions. So, if you get a trashed ROM, you can reboot to recovery, and restore the backup of the boot and data partitions. In some cases, you might also need to reflash the ROM.
+ Magisk and rooting issues have nothing to do with OrangeFox. Please do not come to OrangeFox groups to ask for support about Magisk or any other kind of rooting system. If you do, all you will get is a warning.

## Do I need to flash a "Mount system" zip when using OrangeFox?
No.


## Do I need to flash a "DM-Verity / Forced-Encryption" zip when using OrangeFox?
No.

## Why don't I see System (or Vendor) in the "Wipe" menu?
+ Because the maintainer has decided that you should not try to wipe them manually. 
+ <u>Hint</u>: just leave this issue, and move on with life. There is no reason why you should have to manually wipe system or vendor.

## I set up an OrangeFox password/PIN, but now I have forgotten it
+ That's tough.
+ The moral of story is - <u>do not EVER forget your password/pin</u>. We will certainly not assist you in trying to recover the situation - unless you can `prove beyond a shadow of doubt` that the device is actually yours. Note - you have to <u>prove</u> your ownership. Just asserting that it is yours does not constitute any proof at all.
+ Why? Because if we were simply to assist everyone who comes to our forums saying that they forgot their password/pin, then the OrangeFox security feature is essentially useless.
+ If your device is not maintained, then you are entirely on your own. Do not come to our groups for assistance - that will simply attract a warning or a ban.

## How do I take the OrangeFox logs for posting?
There are a variety of ways:
+ Tap on "*Menu -> More -> Copy Log to SD*", and swipe. This will copy the logs to your internal storage `/sdcard/Fox/` (look for <u>recovery.log</u> there).
+ Connect a PC to your device via adb, and run the command: `adb pull /tmp/recovery.log` (the <u>recovery.log</u> file will be downloaded to the location from which you ran this command, and you can then attach the file to your post)
+ If the boot process gets stuck on the OrangeFox logo, then you should try the `adb pull /tmp/recovery.log` command referred to above, and also try to get a <u>logcat</u>, with `adb shell logcat` or `adb logcat`.
+ For logcats, you can run something like: `adb shell logcat -d >logcat.log` to save the logcat to a file ("logcat.log") on your PC.
+ Send the contents of the device's `/sdcard/Fox/logs/` folder (put them all in a single zip archive) - as long as at least one of them contains a log showing the problem that you have encountered.

## Aroma/AromaFM doesn't work
That is unfortunate - but there is nothing that can be done about it - unless and until somebody decides to update the Aroma project. This also applies to Aroma GAPPs.


## I have encountered a problem while using OrangeFox Recovery
+ Provide the logs (see above for how to do this)
+ Provide a full explanation of precisely what has happened, and precisely how you got to that point.
+ If you do not provide both of these, then we will simply ignore any report of any problem. So, it is advisable to (a) learn how to take logs before trying anything, and (b) always take the logs before rebooting after using the recovery.
+ Note: just posting a screenshot is <u>totally unhelpful</u> - and just saying that it "doesn't work" is totally unhelpful. You **must** post the recovery logs

## How can I know the maintainer for my device?
Tap on *Menu -> (Settings icon) -> About*

## How I can build OrangeFox for my own device?
[See this](/dev/building)