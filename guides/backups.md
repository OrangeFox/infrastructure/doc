---
title: Backups
description: 
published: true
date: 2023-05-20T20:17:34.353Z
tags: 
editor: markdown
dateCreated: 2020-06-26T05:59:59.645Z
---

## I want to make a backup of my ROM. Which partitions must I backup?
+ To make a full backup of your ROM, at the very least, backup these:
#### A. For devices without dynamic partitions
+ <u>Boot</u>
+ <u>Data</u>
+ <u>System_Image</u> (or System - but <u>only</u> if System_Image is not available for backup), and
+ <u>Vendor_Image</u> (or Vendor - but <u>only</u> if Vendor_Image is not available for backup) 

#### B. For devices with dynamic partitions
+ <u>Boot</u>
+ <u>Data</u>
+ Perhaps - the <u>Super</u> partition, if desired

#### C. Optional partitions
+ You might also wish to backup the <u>Internal Storage</u>. 
+ You should also have at least one good backup of *<u>Persist_Image</u>*. Note that you should <u>not</u> restore a backup of the persist image, unless it proves absolutely necessary (and it would be highly exceptional for this to be the case). This backup is only an *insurance policy* for the extremely rare situations when disaster has already struck, <u>and</u> all well-informed advice is to the effect that <u>only</u> restoring "persist" can fix the problem.

<b>NOTES</b>: 
+ Restoring a backup of a partition (including internal storage) means that anything that is currently on that partition <u>will be wiped</u>, and will be <u>replaced with the contents of the backup</u>. So, if there is anything that you would rather keep on any partition, then you <u>MUST NOT</u> restore a backup of that partition - otherwise you <u>will</u> lose the current contents.
+ If you have a modern ROM (ie, if the underlying Android version is higher than Oreo) then do NOT backup System or Vendor, or else you <u>will</u> have problems when restoring. You must only backup System_Image or Vendor_Image.

## Why should I make image backups for older devices, when they take up so much space?
+ It is true that image backups can take much more space than filesystem backups. So, for example, a backup of "system image" will type typically be much bigger than a backup of "system" (same for "vendor image" versus "vendor"). But there are many reasons why an image backup is <u>superior</u>:
1. A backup of system image (or vendor image) is a perfect bit-for-bit copy (ie, image) of the partition. So when the backup is restored, what is restored is an <u>exact copy</u>, to the very last byte, of what was there before. You are therefore unlikely to experience any problem after restoring an image backup.
2. On the other hand, a simple backup of system (or vendor) is just a tar archive of the filesystem's contents. Restoring such a backup can be problematic. While it may be fine on very old devices running very old ROMs (eg, based on Android 6, or Nougat) with newer devices and ROMs, you will likely just get a *bootloop* (or worse) after restoring, as you run into dm-verity or other like issues.
3. If you are running MIUI 11 or MIUI 12, you really do <u>NOT</u> want to backup or restore system or vendor. Trying to do that is likely to end in tears.
4. So, in light of all the above, you would be well advised to <u>make image backups only</u> where that option is available. If the larger backup size means that you need to get a bigger MicroSD card, or a bigger USB pen drive to use with OTG, so be it. It is a price well worth paying.

## Some backup tips
+ You probably only really need to backup your <u>data partition</u> (and perhaps the <u>internal storage</u>) via a Recovery. While this may seem a bit unusual, you really should <u>always</u> keep a copy of your full ROM's zip installer. By keeping a copy of your ROM's zip installer, restoring the ROM would be a simple matter of formatting data, clean-flashing the ROM, booting up the newly flashed ROM (to make sure that it actually boots, and to let it encrypt the storage in its own way), and then rebooting to  recovery mode. This would give you a working device, and a fresh copy of the boot, system, and vendor partitions. Then you only need restore the backed up data (and internal storage), and you have your system again.
+ Do not rely entirely on a nandroid (ie, Recovery) backup of your internal storage (ie, your user data, photos, etc). It is wise to have copies in the cloud, and/or on your PC.
+ Do not rely entirely on a nandroid (ie, Recovery) backup of your data partition. It is wise to let your ROM backup your settings and apps data to the cloud (eg, Google Drive), or use an app to do the same. This provides a second line of defence. Nandroid <u>backups of the data partition are potentially unreliable</u>, and that is just the way it is.
+ Always take a backup of your user data (to an external storage device or the cloud) every time you want to flash something (anything - ROMs, recovery, kernels, mods, OTA updates, or whatever else). `Ignore this advice at your own peril`.
+ Creating backups of encrypted data is fraught with risks. If you want to backup the data partition of an encrypted device, <u>you would be very well advised to first delete the lockscreen password/pin in the ROM before booting to recovery to create the backup</u>. If you do not do this, you might have issues when trying to restore the data backup.
+ Do not try to restore a backed-up data partition from one ROM to another ROM.
+ Do not try to restore a backup of an encrypted device to a device that is not encrypted, or to a device that is encrypted with a different encryption protocol (eg, by a different ROM).
+ Do not try to restore a backup of one ROM on top of another ROM. First flash the ROM zip of the ROM whose backup you want to restore (the <u>precise version</u> that was backed up) before restoring its backup.
+ Do not try to restore a backup of a partition that currently contains data which you want to retain. Restoring a backup of a partition necessarily involves automatic wiping of its current contents, and replacing them with the contents of the backup. So anything already there before restoring the backup will be <u>irretrievably</u> lost.
+ With Android 12 and higher there can be potential fscrypt policy problems that can make the system unbootable after restoring a data backup. Make sure that the ROM you are restoring the backup to is <u>exactly</u> the same as the one that you took the backup from.
+ The Achilles' heel of data backups is that they are just "tar" archives of the filesystem's contents. Therefore, <u>any</u> issue with even a single file or directory (out of possibly several thousands) is liable to cause the <u>entire</u> backup or restore to fail (the dreaded "error 255" or similar errors). Any issue with encryption, permissions, or sepolicy, is also liable to have the same effect. This kind of problem can avoided with /system or /vendor or /persist by making image backups, instead of filesystem backups. Unfortunately, this option is not available in respect of data backups or internal storage backups.
+ Conclusion: `data backups are potentially unreliable`. So, do <u>not</u> rely on them exclusively. Ignore this advice at your own peril.


## Sometimes I get an "error 255" when trying to backup my data partition
+ This situation can happen for any number of reasons. Do a Google search for this.
+ The only way to ascertain the true cause is to check the recovery log file immediately, and search for the error. Normally the log would show where the error happened. 
+ Sometimes the error happens because of a file that is too large. If this is the case, then you will need to delete the file. If you really need to keep the file, then you are basically stuck.
+ Sometimes the error happens because of the presence of a problematic directory: e.g., `/data/per_boot` or `/data/fonts/files`, etc. In such a case, you will need to remove the problematic directory before trying to create the backup. There are some other directories that may cause the same problem. The solution is the same - delete the directory before starting your backup.
+ Sometimes the problem happens because you have created multiple users on the ROM - in this case, you have to delete all the extra users
+ Sometimes the problem happens because you use parallel apps - in this case, you need to stop using that
+ If the problem is caused by something else, then you will need to report that to the device's maintainer - and *you must include the recovery log* in the report.

## How do I restore my backup?
This is the process:
+ There are <u>precautions</u> that you should take <u>before</u> starting the restore process (obviously, your backup <u>must</u> be on a MicroSD card or some other <u>external</u> storage medium (eg, USB-OTG) - `NOT` on the internal storage):
+ 1. If the device <u>is encrypted</u>, then you must <u>**format**</u> the data partition (NOTE: **format** - not just wipe). If the device is <u>not encrypted</u>, then you can just wipe the data partition (ie, no need to format)
+ 2. Reboot OrangeFox
+ 3. Flash the original zip of the ROM whose backup you are trying to restore
+ 4. Wipe cache and dalvik
+ 5. Reboot to system to ensure that the newly-flashed ROM actually boots (and that, if encrypted before, to allow it to re-encrypt with its own encryption - but do `NOT` set any pin/password at this point). 

+ <u>NOTE</u>: *If the device just reboots back to recovery instead of booting to system, format the data partition again, and then reboot to system again*;

+ 6. After the ROM has booted properly, reboot to OrangeFox
+ 7. Tap on the Backup icon
+ 8. Select the partition(s) that you wish to restore
+ 9. Swipe to restore
+ 10. Reboot to system

+<u>NOTE</u>: *If the device gets stuck when trying to boot to system, then something has gone wrong with the restoration - perhaps a bad backup or a borked restore. It may be possible to take a logcat at this point to see what is the problem. But ultimately you might have to format the data partition again*.

## I get an "error 255" when trying to restore my data backup
+ This is an old TWRP problem (do a Google search for this), which can be caused by a number of different things (eg, corrupted backups, selinux policy issues, multi-user or parallel apps stuff being backed up, trying to restore a backup of an encrypted device to a newly flashed ROM without first formatting data and booting up the ROM after flashing it, etc, etc).
+ If you get this error while trying to restore a data partition backup, follow the steps outlined in the "*How do I restore my backup*" section above. 
+ If this does not resolve the problem, then you are stuck, as far as restoring via recovery is concerned.
+ In that situation, your best option is to copy the backed up data files ("data.ext.win000", etc) to your PC, and then try to extract the files with a program that can extract "tar" archives (eg, "tar" on linux, "WinRAR" on Windows, etc). It may be possible to get access to many of your backed up files using this method. If this does not work, then there is nothing else that you can do.

**NOTE**: if you get this error because you tried to restore a backup immediately after flashing a ROM, then you should clean-flash the ROM again, then boot up the ROM and do some simple configurations (but do `NOT` set any pin/passowrd at this point), then reboot to recovery, then try to restore the backup again. This might well solve the problem. If it doesn't solve the problem, then you're out of luck.

## Can I restore TWRP backups with OrangeFox?
+ This <u>may</u> be possible, depending on a number of factors:
+ A. The partitions list of the TWRP backups must be compatible with OrangeFox's partitions list
+ B. TWRP looks for its backups in the "<u>TWRP/BACKUPS/</u>" folder, while OrangeFox looks for its backups in the "<u>Fox/BACKUPS/</u>" folder - so you will have to rename the TWRP folder before OrangeFox can find it
+ Whether the attempt to restore will work is a matter of trial and error. You are welcome to try it. If it works, fine. If it doesn't work, there is nothing that we can do about it.

